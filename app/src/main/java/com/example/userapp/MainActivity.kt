package com.example.userapp

import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.util.prefs.AbstractPreferences

class MainActivity : AppCompatActivity() {
    private lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        read()
    }


    private fun init(){
        sharedPreferences = getSharedPreferences("userData", MODE_PRIVATE)

    }



    fun save(view: View){
        val email = emailEditText.text.toString()
        val firstName = firstNameEditText.text.toString()
        val lastName = lastNameEditText.text.toString()
        val age = ageEditText.text.toString().toInt()
        val address = addressEditText.text.toString()

        val edit = sharedPreferences.edit()
        if(email.isNotEmpty() && firstName.isNotEmpty() &&
                lastName.isNotEmpty() && age.toString().isNotEmpty() &&
                address.isNotEmpty()) {
            edit.putString("email", email)
            edit.putString("firstName", firstName)
            edit.putString("lastName", lastName)
            edit.putInt("age", age)
            edit.putString("address", address)
            edit.apply()

        }else{
            Toast.makeText(this,"Please fill all fields!",Toast.LENGTH_SHORT).show()
        }

    }
    fun read(){
        val email = sharedPreferences.getString("email","")
        val firstName = sharedPreferences.getString("firstName","")
        val lastName = sharedPreferences.getString("lastName","")
        val age = sharedPreferences.getInt("age",0)
        val address = sharedPreferences.getString("address","")

        emailEditText.setText(email)
        firstNameEditText.setText(firstName)
        lastNameEditText.setText(lastName)
        ageEditText.setText(age.toString())
        addressEditText.setText(address)



    }


}